﻿using System.Net;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System;


namespace CBR_to_SQL
{
    class Program
    {
        static void Main(string[] args)
        {
            Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            CBRrequest();
            List<Valute> valutes = parseXML();
            UpdateDatabase(valutes);
        }

        static void CBRrequest()
        {
            WebRequest request = WebRequest.Create("http://www.cbr.ru/scripts/XML_daily.asp?date_req=" + System.DateTime.UtcNow.AddHours(3).ToString("d"));

            try
            {
                WebResponse response = request.GetResponse();
                using (Stream stream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        File.WriteAllText("XML_daily.xml", reader.ReadToEnd());
                    }
                }
            }
            catch (WebException e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        static List<Valute> parseXML()
        {
            List<Valute> valutes = new List<Valute>();

            XmlDocument doc = new XmlDocument();
            doc.Load("XML_daily.xml");
            XmlElement root = doc.DocumentElement;

            string date = root.Attributes.GetNamedItem("Date").Value;

            foreach (XmlNode node in root)
            {
                string charCode = null;
                float value = 0;
                foreach (XmlNode childNode in node)
                {
                    if (childNode.Name == "CharCode")
                        charCode = childNode.InnerText;

                    if (childNode.Name == "Value")
                        value = float.Parse(childNode.InnerText);
                }

                valutes.Add(new Valute(charCode, value, Convert.ToDateTime(date)));
            }

            return valutes;
        }

        static void UpdateDatabase(List<Valute> valutes)
        {
            using (ApplicationContext context = new ApplicationContext())
            {
                foreach (Valute valute in valutes)
                    context.Valutes.Add(valute);
                try
                {
                    context.SaveChanges();
                }
                catch(Microsoft.EntityFrameworkCore.DbUpdateException e)
                {
                    Console.WriteLine(e.Message);
                    Console.ReadLine();
                }
            }
        }
    }
}
