﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace CBR_to_SQL
{
    [Table("Курс")]
    class Valute
    {
        [Column("Дата", TypeName = "date")]
        public DateTime Date { get; set; }

        [Column("Валюта")]
        public string CharCode { get; set; }

        [Column("Курс")]
        public float Value { get; set; }

        public Valute(string charCode, float value, DateTime date)
        {
            CharCode = charCode;
            Value = value;
            Date = date;
        }
    }
}
