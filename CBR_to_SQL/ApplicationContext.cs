﻿using Microsoft.EntityFrameworkCore;

namespace CBR_to_SQL
{
    class ApplicationContext : DbContext
    {
        public DbSet<Valute> Valutes { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Valute>().HasKey(v => new { v.Date, v.CharCode });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=CentralBank;Trusted_Connection=True;");
        }
    }
}
